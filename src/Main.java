import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Rectangle a = new Rectangle(8,5 ,new Point(9,6));
        System.out.println("\nArea of the rectangle:"+ a.area());
        System.out.println("Perimeter of the rectangle:"+ a.perimeter());
        System.out.println(Arrays.deepToString(a.corners()));


        Circle b = new Circle(10,new Point(10,15));

        System.out.println("Area of the circle:"+b.area());
        System.out.println("Perimeter of the circle:"+b.perimeter());


        Circle c = new Circle(5,new Point(10,15));


        System.out.println(b.intersect(b,c));


    }
}