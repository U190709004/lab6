public class Circle {
    public int radius;
    public Point center;

    public double area(){
        return Math.PI*radius*radius;
    }

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double perimeter(){
        return Math.PI*2*radius;
    }
    public  boolean intersect(Circle b,Circle c ){
        double C1C2;
        C1C2 = Math.sqrt(Math.pow((b.center.xCoord- c.center.xCoord),2)+Math.pow((b.center.yCoord- c.center.yCoord),2));

        return !(C1C2 < b.radius + c.radius) || !(Math.abs(b.radius - c.radius) > C1C2);
    }

}